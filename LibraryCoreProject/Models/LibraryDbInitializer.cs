﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.Models
{
    public static class LibraryDbInitializer
    {
        public static void Initialize(ApplicationContext context)
        {
            context.Database.EnsureCreated();
            if (context.Books.Any())
            {
                return;
            }
            var Authors = new Author[]
            {
                new Author { AuthorName = "Л.Н. Толстой" },
                new Author { AuthorName = "А.С. Пушкин" },
                new Author { AuthorName = "А.П. Чехов" },
                new Author { AuthorName = "Д.Ф. Купер" },
                new Author { AuthorName = "А. Сапковский" }
            };
            foreach(var a in Authors)
            {
                context.Authors.Add(a);
            }
            context.SaveChanges();
            var Publishers = new Publisher[]
            {
                new Publisher { PublisherName = "Проспект" },
                new Publisher { PublisherName = "Правда" },
                new Publisher { PublisherName = "Казанское книжное издательство" }

            };
            foreach (var p in Publishers)
            {
                context.Publishers.Add(p);
            }
            context.SaveChanges();
            var Genres = new Genre[]
            {
                new Genre { GenreName = "Исторический роман" },
                new Genre { GenreName = "Сказка" },
                new Genre { GenreName = "Биография" },
                new Genre { GenreName = "Фэнтези" }
            };
            foreach (var g in Genres)
            {
                context.Genres.Add(g);
            }
            context.SaveChanges();
            var Books = new Book[]
            {
                new Book { AuthorId = 1, GenreId = 1, PublisherId =1, Name = "Война и мир" },
                new Book { AuthorId = 2, GenreId = 1, PublisherId =3, Name = "Книга интересная" },
                new Book { AuthorId = 3, GenreId = 2, PublisherId =2, Name = "Книга веселая" },
                new Book { AuthorId = 3, GenreId = 3, PublisherId =3, Name = "Книга хорошая" },
                new Book { AuthorId = 4, GenreId = 4, PublisherId =2, Name = "Книга прикольная" },
                new Book { AuthorId = 5, GenreId = 1, PublisherId =2, Name = "Книга с приколом" },
                new Book { AuthorId = 5, GenreId = 2, PublisherId =1, Name = "Мир и война" }
            };
            foreach (var b in Books)
            {
                context.Books.Add(b);
            }
            context.SaveChanges();
        }
    }
}
