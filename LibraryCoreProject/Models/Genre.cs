﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.Models
{
    public class Genre
    {
        public int GenreId { get; set; }
        [Display(Name="Название жанра:")]
        [Required(ErrorMessage ="Введите название жанра")]
        public string GenreName { get; set; }
        public ICollection<Book> Books { get; set; }
        public Genre()
        {
            Books = new List<Book>();
        }
    }
}
