﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.Models
{
    public class Booking
    {
        public int Id { get; set; }

        [Display(Name = "Дата бронирования")]
        public DateTime BookingTime { get; set; }

        [Display(Name = "Дата выдачи книги")]
        public DateTime? RecievmentTime { get; set; }

        //Дата возвращения книги
        public DateTime? ReturningTime { get; set; }

        public int BookId { get; set; }
        [Display(Name ="Книга")]
        public Book Book { get; set; }

        public string UserId { get; set; }
        [Display(Name ="Пользователь")]
        public User User { get; set; }
    }
}
