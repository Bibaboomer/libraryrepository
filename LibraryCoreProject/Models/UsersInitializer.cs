﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace LibraryCoreProject.Models
{
    public class UsersInitializer
    {
        public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }
        public static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByNameAsync("admin@gmail.com").Result==null)
            {
                User user = new User { Email = "admin@gmail.com", UserName = "admin@gmail.com", EmailConfirmed=true };
                IdentityResult result = userManager.CreateAsync(user, "Admin12345").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "admin").Wait();
                }
            }
            if (userManager.FindByNameAsync("librarian@gmail.com").Result == null)
            {
                User user = new User { Email = "librarian@gmail.com", UserName = "librarian@gmail.com", EmailConfirmed=true };
                IdentityResult result = userManager.CreateAsync(user, "Librarian12345").Result;
                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "librarian").Wait();
                }
            }
        }

        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("admin").Result)
            {
                IdentityRole role = new IdentityRole { Name = "admin" };
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("librarian").Result)
            {
                IdentityRole role = new IdentityRole { Name = "librarian" };
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("user").Result)
            {
                IdentityRole role = new IdentityRole { Name = "user" };
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
        }
    }
}
