﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace LibraryCoreProject.Models
{
    public class User: IdentityUser
    {
        public ICollection<Booking> Bookings { get; set; }
        public ICollection<Subscription> Subscriptions { get; set; }

        public User()
        {
            Bookings = new List<Booking>();
            Subscriptions = new List<Subscription>();
        }
    }
}
