﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.Models
{
    public class Author
    {
        public int AuthorId { get; set; }
        [Display(Name ="Имя автора:")]
        [Required(ErrorMessage ="Введите имя автора")]
        public string AuthorName { get; set; }
        public ICollection<Book> Books { get; set; }
        public Author()
        {
            Books = new List<Book>();
        }

    }
}
