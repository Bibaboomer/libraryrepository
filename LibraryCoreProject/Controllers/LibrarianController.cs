﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LibraryCoreProject.Controllers
{
    public class LibrarianController : Controller
    {
        IUnitOfWork unitOfWork;
        public LibrarianController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #region Список зарезервированных книг
        public ActionResult ReservationList()
        {
            // Забронированные, но не выданные книги

            IEnumerable<Booking> bookings = unitOfWork.Bookings.GetReservationsList();

            return View(bookings);
        }

        #endregion

        #region Список выданных книг

        public ActionResult BookingList()
        {
            // Выданные, но не возвращенные книги

            IEnumerable<Booking> bookings = unitOfWork.Bookings.GetBookingsList();

            return View(bookings);
        }

        #endregion


        #region Выдать книгу

        public ActionResult GiveBook(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);
            booking.RecievmentTime = DateTime.Now;

            unitOfWork.Bookings.Update(booking);
            unitOfWork.Save();

            return RedirectToAction("BookingList");
        }

        #endregion


        #region Получить книгу
        public async Task<IActionResult> ReturnBook(int Id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(Id);
            booking.ReturningTime = DateTime.Now;

            Book book = unitOfWork.Books.GetItem(booking.BookId);
            book.isAvailable = true;

            unitOfWork.Bookings.Update(booking);
            unitOfWork.Books.Update(book);
            unitOfWork.Save();

            IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

            SendSubscriptionsEmail(subscriptions);


            return RedirectToAction("BookingList");

        }
        #endregion

        public async void SendSubscriptionsEmail(IEnumerable<Subscription> subscriptions)
        {
            Tools.EmailService emailService = new Tools.EmailService();
            foreach (var s in subscriptions)
            {
                string email = s.User.Email;
                string subject = "Книга на обновления которой Вы подписались снова доступна.";
                string message = $"Книга {s.Book.Name} доступна для бронирования.";
                await emailService.SendEmailAsync(email, subject, message);
            }
        }
    }
}