﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using LibraryCoreProject.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LibraryCoreProject.Controllers
{
    [Authorize(Roles="admin,librarian")]
    public class BookController: Controller
    {
        private readonly IUnitOfWork unitOfWork;
        public BookController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        
        public ActionResult BookList(int? AuthorId, int? GenreId, int? PublihserId, string SearchString = null)
        {
            List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();
            List<Book> books = unitOfWork.Books.GetFilteredList(AuthorId, GenreId, PublihserId, SearchString).ToList();

            BooksFilterViewModel model = new BooksFilterViewModel
            {
                Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                Genres = new SelectList(genres, "GenreId", "GenreName"),
                Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                SearchString = String.Empty,
                Books=books
            };

            return View(model);
            
        }
        #region Добавление книги
        [HttpGet]
        public ActionResult AddBook()
        {
            List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();

            AddBookViewModel model = new AddBookViewModel
            {
                Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                Genres = new SelectList(genres, "GenreId", "GenreName"),
                Publishers = new SelectList(publishers, "PublisherId", "PublisherName")
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult AddBook(AddBookViewModel model)
        {
            if (ModelState.IsValid)
            {
                Author author;
                Genre genre;
                Publisher publisher;

                if (model.isNewAuthor)
                {
                    author = new Author { AuthorName = model.AuthorName };
                    unitOfWork.Authors.Create(author);
                    unitOfWork.Save();
                }
                else
                {
                    author = unitOfWork.Authors.GetItem(model.AuthorId);
                }

                if (model.isNewGenre)
                {
                    genre = new Genre { GenreName = model.GenreName };
                    unitOfWork.Genres.Create(genre);
                    unitOfWork.Save();
                }
                else
                {
                    genre = unitOfWork.Genres.GetItem(model.GenreId);
                }

                if (model.isNewPublisher)
                {
                    publisher = new Publisher { PublisherName = model.PublisherName };
                    unitOfWork.Publishers.Create(publisher);
                    unitOfWork.Save();
                }
                else
                {
                    publisher = unitOfWork.Publishers.GetItem(model.PublisherId);
                }

                Book book = new Book
                {
                    Name = model.Name,
                    AuthorId = author.AuthorId,
                    PublisherId = publisher.PublisherId,
                    GenreId = genre.GenreId
                };

                unitOfWork.Books.Create(book);
                unitOfWork.Save();

                return RedirectToAction("BookList");
               
            }
            else
            {
                var authors = unitOfWork.Authors.GetItemList();
                var genres = unitOfWork.Genres.GetItemList();
                var publishers = unitOfWork.Publishers.GetItemList();


                model.Authors = new SelectList(authors, "AuthorId", "AuthorName");
                model.Genres = new SelectList(genres, "GenreId", "GenreName");
                model.Publishers = new SelectList(publishers, "PublisherId", "PublisherName");

                return View(model);
            }
        }


       
        #endregion
        #region Редактирование книги
        [HttpGet]
        public ActionResult EditBook(int id)
        {
            if (unitOfWork.Books.GetItem(id) != null)
            {
                Book book = unitOfWork.Books.GetItem(id);
                List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
                List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
                List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();

                AddBookViewModel model = new AddBookViewModel
                {
                    BookId = book.Id,
                    Name = book.Name,
                    AuthorId = book.AuthorId,
                    GenreId = book.GenreId,
                    PublisherId = book.PublisherId,

                    Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                    Genres = new SelectList(genres, "GenreId", "GenreName"),
                    Publishers = new SelectList(publishers, "PublisherId", "PublisherName")
                };
                return View(model);
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPost]
        public IActionResult EditBook(AddBookViewModel model)
        {
            if (ModelState.IsValid)
            {
                Author author = new Author();
                Genre genre = new Genre();
                Publisher publisher = new Publisher();

                if (model.isNewAuthor)
                {
                    author = new Author { AuthorName = model.AuthorName };
                    unitOfWork.Authors.Create(author);
                    unitOfWork.Save();
                }
                else
                {
                    author = unitOfWork.Authors.GetItem(model.AuthorId);
                }

                if (model.isNewGenre)
                {
                    genre = new Genre { GenreName = model.GenreName };
                    unitOfWork.Genres.Create(genre);
                    unitOfWork.Save();
                }
                else
                {
                    genre = unitOfWork.Genres.GetItem(model.GenreId);
                }

                if (model.isNewPublisher)
                {
                    publisher = new Publisher { PublisherName = model.PublisherName };
                    unitOfWork.Publishers.Create(publisher);
                    unitOfWork.Save();
                }
                else
                {
                    publisher = unitOfWork.Publishers.GetItem(model.PublisherId);
                }

                Book book = unitOfWork.Books.GetItem(model.BookId);
                book.Name = model.Name;
                book.PublisherId = publisher.PublisherId;
                book.AuthorId = author.AuthorId;
                book.GenreId = genre.GenreId;

                unitOfWork.Books.Update(book);
                unitOfWork.Save();

                return RedirectToAction("BookList");
            }
            else
            {
                Book book = unitOfWork.Books.GetItem(model.BookId);

                var authors = unitOfWork.Authors.GetItemList();
                var genres = unitOfWork.Genres.GetItemList();
                var publishers = unitOfWork.Publishers.GetItemList();

                model.Authors = new SelectList(authors, "AuthorId", "AuthorName");
                model.Genres = new SelectList(genres, "GenreId", "GenreName");
                model.Publishers = new SelectList(publishers, "PublisherId", "PublisherName");

                return View(model);
            }

        }
        #endregion

        #region Удаление книги
        [HttpGet]
        public ActionResult DeleteBook(int id)
        {
            if (unitOfWork.Books.GetBookExtended(id) != null)
            {
                Book book = unitOfWork.Books.GetBookExtended(id);
                DeleteBookViewModel model = new DeleteBookViewModel
                {
                    BookId = book.Id,
                    BookName = book.Name,
                    AuthorName = book.Author.AuthorName,
                    GenreName = book.Genre.GenreName,
                    PublisherName = book.Publisher.PublisherName
                };
                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost, ActionName("DeleteBook")]
        public ActionResult DeleteBookConfirmed(int id)
        {
            unitOfWork.Books.Delete(id);
            unitOfWork.Save();
            return RedirectToAction("BookList");
        }
        #endregion
    }
}
