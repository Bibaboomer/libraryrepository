﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using LibraryCoreProject.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LibraryCoreProject.Controllers
{
    public class CatalogController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        public CatalogController(IUnitOfWork unitOfWork, UserManager<User> userManager)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
        }
        public ActionResult Index(int? AuthorId, int? GenreId, int? PublihserId, string SearchString = null)
        {
            List<Author> authors = unitOfWork.Authors.GetItemList().ToList();
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();
            List<Book> books = unitOfWork.Books.GetFilteredList(AuthorId, GenreId, PublihserId, SearchString).ToList();
            User user;
            if (User.Identity.IsAuthenticated)
            {
                user = userManager.GetUserAsync(HttpContext.User).Result;
            }
            else
            {
                user = null;
            }

            BooksFilterViewModel model = new BooksFilterViewModel
            {
                Authors = new SelectList(authors, "AuthorId", "AuthorName"),
                Genres = new SelectList(genres, "GenreId", "GenreName"),
                Publishers = new SelectList(publishers, "PublisherId", "PublisherName"),
                SearchString = String.Empty,
                Books = books,
                User = user


            };

            return View(model);
        }

        [Authorize]
        public ActionResult ReserveBook(int BookId, string UserId)
        {
            if (UserId == null)
            {
                RedirectToAction("Login", "Account");
            }
            

            Book book = unitOfWork.Books.GetItem(BookId);

            User user = userManager.FindByIdAsync(UserId).Result;

            if (!user.EmailConfirmed)
            {
                RedirectToPage("ConfirmEmail");
            }

            Booking booking = new Booking { BookId = BookId, BookingTime = DateTime.Now, User = user };
            unitOfWork.Bookings.Create(booking);

            book.isAvailable = false;

            unitOfWork.Books.Update(book);
            unitOfWork.Save();

            return View(book);
        }
        #region Подписка
        [Authorize]
        public ActionResult Subscribe(string UserId, int BookId)
        {
            unitOfWork.Subscriptions.Create(new Subscription
            {
                UserId = UserId,
                BookId = BookId
            });

            Book book = unitOfWork.Books.GetItem(BookId);

            unitOfWork.Save();

            return View(book);
        }

        #endregion
    }
}