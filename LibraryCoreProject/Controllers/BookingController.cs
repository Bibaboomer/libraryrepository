﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using LibraryCoreProject.ViewModels;

namespace LibraryCoreProject.Controllers
{
    public class BookingController: Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly UserManager<User> userManager;
        public BookingController(IUnitOfWork unitOfWork, UserManager<User> userManager)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
        }

        #region Зарезервированные книги пользователя

        public ActionResult MyBookings()
        {
            if(!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            string userId = userManager.GetUserAsync(HttpContext.User).Result.Id;
            User user = userManager.Users.FirstOrDefault(u => u.Id == userId);
            List<Booking> reservations = unitOfWork.Bookings.GetReservationsList(userId).ToList();
            List<Booking> bookings = unitOfWork.Bookings.GetBookingsList(userId).ToList();
            MyBookingsViewModel model = new MyBookingsViewModel
            {
                Reservations = reservations,
                Bookings =bookings
            };
            return View(model);
        }
        #endregion

        #region Отменить бронь
        [HttpGet]
        public ActionResult CancelBooking(int id)
        {
            if (unitOfWork.Bookings.GetItem(id) != null)
            {
                Booking booking = unitOfWork.Bookings.GetItem(id);
                return View(booking);
            }
            return NotFound();
        }
        [HttpPost, ActionName("CancelBooking")]
        public ActionResult CancelBookingConfirmed(int id)
        {
            Booking booking = unitOfWork.Bookings.GetItem(id);
            Book book = unitOfWork.Books.GetItem(booking.BookId);

            book.isAvailable = true;

            unitOfWork.Books.Update(book);

            unitOfWork.Bookings.Delete(id);

            unitOfWork.Save();

            IEnumerable<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id);

            SendSubscriptionsEmail(subscriptions);
           

            return RedirectToAction("MyBookings");
        }
        #endregion

        public async void SendSubscriptionsEmail(IEnumerable<Subscription> subscriptions)
        {
            Tools.EmailService emailService = new Tools.EmailService();
            foreach (var s in subscriptions)
            {
                string email = s.User.Email;
                string subject = "Книга на обновления которой Вы подписались снова доступна.";
                string message = $"Книга {s.Book.Name} доступна для бронирования.";
                await emailService.SendEmailAsync(email, subject, message);
            }
        }

    }
}
