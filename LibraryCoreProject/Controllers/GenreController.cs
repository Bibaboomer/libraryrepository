﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;

namespace LibraryCoreProject.Controllers
{
    public class GenreController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public GenreController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Genre
        public ActionResult Index()
        {
            List<Genre> genres = unitOfWork.Genres.GetItemList().ToList();
            return View(genres);
        }

        // GET: Genre/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Genre/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Genre genre)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Genres.Create(genre);
                unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(genre);
        }

        // GET: Genre/Edit/5
        public ActionResult Edit(int id)
        {
            var genre = unitOfWork.Genres.GetItem(id);
            if (genre == null)
            {
                return NotFound();
            }
            return View(genre);
        }

        // POST: Genre/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Genre genre)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Genres.Update(genre);
                unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(genre);
        }

        // GET: Genre/Delete/5
        public ActionResult Delete(int id)
        {
            var genre = unitOfWork.Genres.GetItem(id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // POST: Genre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            unitOfWork.Genres.Delete(id);
            unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }
    }
}
