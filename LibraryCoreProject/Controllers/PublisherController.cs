﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;

namespace LibraryCoreProject.Controllers
{
    public class PublisherController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public PublisherController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Publisher
        public ActionResult Index()
        {
            List<Publisher> publishers = unitOfWork.Publishers.GetItemList().ToList();
            return View(publishers);
        }

        // GET: Publisher/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Publisher/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Publisher publisher)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Publishers.Create(publisher);
                unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(publisher);
        }

        // GET: Publisher/Edit/5
        public ActionResult Edit(int id)
        {
            var publisher = unitOfWork.Publishers.GetItem(id);
            if (publisher == null)
            {
                return NotFound();
            }
            return View(publisher);
        }

        // POST: Publisher/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Publisher publisher)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Publishers.Update(publisher);
                unitOfWork.Save();
            
                return RedirectToAction(nameof(Index));
            }
            return View(publisher);
        }

        // GET: Publisher/Delete/5
        public ActionResult Delete(int id)
        {
            var publisher = unitOfWork.Publishers.GetItem(id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // POST: Publisher/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            unitOfWork.Publishers.Delete(id);
            unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }
    }
}
