﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;

namespace LibraryCoreProject.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public AuthorController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Author
        public ActionResult Index()
        {
            List<Author> authors = unitOfWork.Authors.GetItemList().ToList(); 
            return View(authors);
        }

        // GET: Author/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Author author)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Authors.Create(author);
                unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(author);
        }

        // GET: Author/Edit/5
        public ActionResult Edit(int id)
        {
            var author =unitOfWork.Authors.GetItem(id);
            if (author == null)
            {
                return NotFound();
            }
            return View(author);
        }

        // POST: Author/Edit/5
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Author author)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Authors.Update(author);
                unitOfWork.Save();

                return RedirectToAction("Index");
            }
            return View(author);
        }

        // GET: Author/Delete/5
        public ActionResult Delete(int id)
        {
            var author = unitOfWork.Authors.GetAuthorWithBooks(id);
                
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }

        // POST: Author/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            unitOfWork.Authors.Delete(id);
            unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }
    }
}
