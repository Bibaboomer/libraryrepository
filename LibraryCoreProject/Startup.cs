using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using LibraryCoreProject.Tools;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LibraryCoreProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(Configuration.GetConnectionString("NpgsqlConnection")));
            services.AddIdentity<User, IdentityRole>(options=> 
            {
                options.Password.RequiredLength = 10;
                options.Password.RequireNonAlphanumeric = false;

                //options.SignIn.RequireConfirmedEmail = true;
            }).AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IBookRepository, PostgreBookRepository>();
            services.AddTransient<IAuthorRepository, PostgreAuthorRepository>();
            services.AddTransient<IBookingRepository, PostgreBookingRepository>();
            services.AddTransient<ISubscriptionRepository, PostgreSubscriptionRepository>();
            services.AddTransient<IRepository<Genre>, PostgreGenreRepository>();
            services.AddTransient<IRepository<Publisher>, PostgrePublisherRepository>();

            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            UsersInitializer.SeedData(userManager, roleManager);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Catalog}/{action=Index}/{id?}");
            });
        }
    }
}
