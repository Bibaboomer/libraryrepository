﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using LibraryCoreProject.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LibraryCoreProject.Tools
{
    public class TimedHostedService : IHostedService, IDisposable
    {
        private Timer timer;
        private readonly IServiceScopeFactory scopeFactory;

        public TimedHostedService(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(Dowork, null, TimeSpan.Zero, TimeSpan.FromMinutes(2) );
            return Task.CompletedTask;
        }

        private void Dowork(object state)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationContext>();
                UnitOfWork unitOfWork = new UnitOfWork(context);

                IEnumerable<Booking> reservations = unitOfWork.Bookings.GetTimedOutReservationsList().ToList();

                foreach (var reservation in reservations)
                {
                    Book book = unitOfWork.Books.GetItem(reservation.BookId);

                    book.isAvailable = true;

                    unitOfWork.Books.Update(book);

                    unitOfWork.Bookings.Delete(reservation.Id);

                    unitOfWork.Save();

                    List<Subscription> subscriptions = unitOfWork.Subscriptions.GetItemList(book.Id).ToList();

                    SendSubscriptionsEmail(subscriptions);


                }
            }
            
            
        }
        public async void SendSubscriptionsEmail(List<Subscription> subscriptions)
        {
            Tools.EmailService emailService = new Tools.EmailService();
            foreach (var s in subscriptions)
            {
                string email = s.User.Email;
                string subject = "Книга на обновления которой Вы подписались снова доступна.";
                string message = $"Книга {s.Book.Name} доступна для бронирования.";
                await emailService.SendEmailAsync(email, subject, message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
