﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name ="E-mail:")]
        public string Email { get; set; }

        [Required]
        [Display(Name ="Пароль:")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name ="Запомнить?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
