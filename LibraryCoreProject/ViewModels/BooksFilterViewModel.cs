﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LibraryCoreProject.ViewModels
{
    public class BooksFilterViewModel
    {
        [Display(Name="Автор:")]
        public SelectList Authors { get; set; }
        [Display(Name = "Жанр:")]
        public SelectList Genres { get; set; }
        [Display(Name = "Издатель:")]
        public SelectList Publishers { get; set; }
        public string SearchString { get; set; }
        public List<Book> Books { get; set; }
        public User User { get; set; }
    }
}
