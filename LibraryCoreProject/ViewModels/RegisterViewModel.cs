﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name ="E-mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name ="Пароль:")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name ="Подтвердите пароль:")]
        [Compare("Password",ErrorMessage ="Пароли не совпадают")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }

    }
}
