﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.ViewModels
{
    public class MyBookingsViewModel
    {
        public List<Booking> Reservations { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}
