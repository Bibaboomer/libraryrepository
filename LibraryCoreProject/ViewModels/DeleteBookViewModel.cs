﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryCoreProject.ViewModels
{
    public class DeleteBookViewModel
    {
        public int BookId { get; set; }
        [Display(Name="Название книги:")]
        public string BookName { get; set; }
        [Display(Name ="Жанр:")]
        public string GenreName { get; set; }
        [Display(Name = "Автор:")]
        public string AuthorName { get; set; }
        [Display(Name = "Издатель:")]
        public string PublisherName { get; set; }
    }
}
