﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LibraryCoreProject.ViewModels
{
    public class AddBookViewModel : IValidatableObject
    {
        public int BookId { get; set; }
        [Required(ErrorMessage = "Введите название книги")]
        [Display(Name = "Название")]
        public string Name { get; set; }


        public bool isNewAuthor { get; set; }
        public int AuthorId { get; set; }
        public IEnumerable<SelectListItem> Authors { get; set; }
        public string AuthorName { get; set; }


        public bool isNewGenre { get; set; }
        public int GenreId { get; set; }
        public IEnumerable<SelectListItem> Genres { get; set; }
        public string GenreName { get; set; }

        public bool isNewPublisher { get; set; }
        public int PublisherId { get; set; }
        public IEnumerable<SelectListItem> Publishers { get; set; }
        public string PublisherName { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if( isNewAuthor && string.IsNullOrEmpty(AuthorName))
            {
                errors.Add(new ValidationResult("Введите имя автора", new[] {"AuthorName" }));
            }

            if( isNewGenre && string.IsNullOrEmpty(GenreName))
            {
                errors.Add(new ValidationResult("Введите название жанра", new[] { "GenreName" }));
            }

            if (isNewPublisher && string.IsNullOrEmpty(PublisherName))
            {
                errors.Add(new ValidationResult("Введите название издателя", new[] { "PublisherName" }));
            }

            return errors;
        }
    }
}
