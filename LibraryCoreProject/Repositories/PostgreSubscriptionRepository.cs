﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryCoreProject.Repositories
{
    public class PostgreSubscriptionRepository : Repository<Subscription>, ISubscriptionRepository
    {
        private ApplicationContext context
        {
            get { return Context as ApplicationContext; }
        }
        public PostgreSubscriptionRepository(ApplicationContext context) :base(context)
        {
            
        }
        public IEnumerable<Subscription> GetItemList(int BookId)
        {
            return context.Subscriptions.Include(s => s.Book).Include(s => s.User).Where(s => s.BookId == BookId);
        }
    }
}
