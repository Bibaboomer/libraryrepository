﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.AspNetCore.Identity;

namespace LibraryCoreProject.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;

            Authors = new PostgreAuthorRepository(_context);
            Bookings = new PostgreBookingRepository(_context);
            Books = new PostgreBookRepository(_context);
            Subscriptions = new PostgreSubscriptionRepository(_context);
            Genres = new Repository<Genre>(_context);
            Publishers = new Repository<Publisher>(_context);

        }

        public IAuthorRepository Authors { get; private set; }
        public IBookingRepository Bookings { get; private set; }
        public IBookRepository Books { get; private set; }
        public ISubscriptionRepository Subscriptions { get; private set; }
        public IRepository<Genre> Genres { get; private set; }
        public IRepository<Publisher> Publishers { get; private set; }


        public void Save()
        {
            _context.SaveChanges();
        }


        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
