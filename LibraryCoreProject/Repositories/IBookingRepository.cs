﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public interface IBookingRepository: IRepository<Booking>
    {
        IEnumerable<Booking> GetReservationsList();
        IEnumerable<Booking> GetReservationsList(string userId);
        IEnumerable<Booking> GetTimedOutReservationsList();
        IEnumerable<Booking> GetBookingsList();
        IEnumerable<Booking> GetBookingsList(string userId);
    }
}
