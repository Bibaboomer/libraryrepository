﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public interface IAuthorRepository: IRepository<Author>
    {
        Author GetAuthorWithBooks(int id);
    }
}
