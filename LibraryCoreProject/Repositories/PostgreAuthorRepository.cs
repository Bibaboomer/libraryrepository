﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryCoreProject.Repositories
{
    public class PostgreAuthorRepository : Repository<Author>, IAuthorRepository
    {
        private ApplicationContext context
        {
            get { return Context as ApplicationContext; }
        }
        public PostgreAuthorRepository(ApplicationContext context):base(context)
        {
                
        }

        public Author GetAuthorWithBooks(int id)
        {
            return context.Authors.Include(a => a.Books).FirstOrDefault(a => a.AuthorId == id);
        }
    }
}
