﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.AspNetCore.Identity;

namespace LibraryCoreProject.Repositories
{
    public interface IUnitOfWork: IDisposable
    {
        IAuthorRepository Authors { get; }
        IBookingRepository Bookings { get; }
        IBookRepository Books { get; }
        ISubscriptionRepository Subscriptions { get; }
        IRepository<Genre> Genres { get; }
        IRepository<Publisher> Publishers { get; }

        void Save();
    }
}
