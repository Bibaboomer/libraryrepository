﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryCoreProject.Repositories
{
    public class PostgreBookingRepository: Repository<Booking>, IBookingRepository
    {
        private ApplicationContext context
        {
            get { return Context as ApplicationContext; }
        }
        public PostgreBookingRepository(ApplicationContext context): base(context)
        {

        }

        public IEnumerable<Booking> GetReservationsList()
        {
            return context.Bookings.Where(b => b.BookingTime != null && b.RecievmentTime == null).Include(b => b.User).Include(b => b.Book);
        }
        public IEnumerable<Booking> GetReservationsList(string userId)
        {
            return context.Bookings.Where(b => b.BookingTime != null && b.RecievmentTime == null).Include(b => b.User).Include(b => b.Book).Where(r=>r.UserId == userId);
        }

        public IEnumerable<Booking> GetTimedOutReservationsList()
        {
            return context.Bookings.Where(b => b.BookingTime.AddMinutes(3) < DateTime.Now && b.RecievmentTime == null);
        }

        public IEnumerable<Booking> GetBookingsList()
        {
            return context.Bookings.Where(b => b.ReturningTime == null && b.RecievmentTime != null).Include(b => b.User).Include(b => b.Book);
        }
        public IEnumerable<Booking> GetBookingsList(string userId)
        {
            return context.Bookings.Where(b => b.ReturningTime == null && b.RecievmentTime != null).Include(b => b.User).Include(b => b.Book).Where(b=>b.UserId == userId);
        }
    }
}
