﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public class PostgrePublisherRepository:Repository<Publisher>
    {
        public PostgrePublisherRepository(ApplicationContext context) :base(context)
        {

        }
    }
}
