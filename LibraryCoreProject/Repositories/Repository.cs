﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryCoreProject.Repositories
{
    public class Repository<T> : IRepository<T> where T:class
    {
        protected readonly ApplicationContext Context;

        public Repository(ApplicationContext context)
        {
            Context = context;
        }
        public void Create(T item)
        {
            Context.Set<T>().Add(item);
        }

        public void Delete(int id)
        {
            T item = Context.Set<T>().Find(id);
            Context.Set<T>().Remove(item);
        }

        public T GetItem(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetItemList()
        {
            return Context.Set<T>().ToList();
        }

        public void Update(T item)
        {
            Context.Entry(item).State = EntityState.Modified;
        }
    }
}
