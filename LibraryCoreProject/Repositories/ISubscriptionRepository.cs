﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public interface ISubscriptionRepository : IRepository<Subscription>
    {
        IEnumerable<Subscription> GetItemList(int BookId);

    }
}
