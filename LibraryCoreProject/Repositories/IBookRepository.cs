﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public interface IBookRepository: IRepository<Book>
    {
        IEnumerable<Book> GetFilteredList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null);
        Book GetBookExtended(int id);
    }
}
