﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryCoreProject.Repositories
{
    public class PostgreBookRepository : Repository<Book>, IBookRepository
    {
        private ApplicationContext context
        {
            get { return Context as ApplicationContext; }
        }


        public PostgreBookRepository(ApplicationContext context) : base(context)
        {
        }

        public Book GetBookExtended(int id)
        {
            Book book = context.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher).FirstOrDefault(b => b.Id == id);
            return book;
        }

        public IEnumerable<Book> GetFilteredList(int? AuthorId, int? GenreId, int? PublisherId, string searchString = null)
        {
            IEnumerable<Book> books = context.Books.Include(b => b.Author).Include(b => b.Genre).Include(b => b.Publisher);

            if (AuthorId != null && AuthorId != 0)
            {
                books = books.Where(b => b.AuthorId == AuthorId);
            }
            if (GenreId != null && GenreId != 0)
            {
                books = books.Where(b => b.GenreId == GenreId);
            }
            if (PublisherId != null && PublisherId != 0)
            {
                books = books.Where(b => b.PublisherId == PublisherId);
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(b => b.Name.ToLower().Contains(searchString.ToLower()));
            }

            return books;
        }
    }
}
