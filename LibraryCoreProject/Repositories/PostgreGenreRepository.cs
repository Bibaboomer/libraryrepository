﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryCoreProject.Models;

namespace LibraryCoreProject.Repositories
{
    public class PostgreGenreRepository: Repository<Genre>
    {
        public PostgreGenreRepository(ApplicationContext context):base(context)
        {

        }
    }
}
